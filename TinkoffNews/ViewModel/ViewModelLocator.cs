﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinkoffNews.ViewModel
{
    class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        /// 
        public const string MainPageKey = "MainPage";
        public const string NewsTextPageKey = "NewsTextPage";
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            NavigationService navigation = new NavigationService();
            navigation.Configure(MainPageKey, typeof(MainPage));
            navigation.Configure(NewsTextPageKey, typeof(View.NewsText));

            SimpleIoc.Default.Register<INavigationService>(() => navigation);
            SimpleIoc.Default.Register<MainPageViewModel>();
            SimpleIoc.Default.Register<NewsTextViewModel>();
        }

        public MainPageViewModel MainPageInstance
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainPageViewModel>();
            }
        }

        public NewsTextViewModel NewsTextPageInstance
        {
            get
            {
                return ServiceLocator.Current.GetInstance<NewsTextViewModel>();
            }
        }

        // <summary>
        // The cleanup.
        // </summary>
        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}
