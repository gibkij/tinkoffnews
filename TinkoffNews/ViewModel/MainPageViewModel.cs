﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TinkoffNews.Converters;
using TinkoffNews.Extension;
using TinkoffNews.Model;
using TinkoffNews.Service;

namespace TinkoffNews.ViewModel
{
    class MainPageViewModel : ViewModelBase, INavigable
    {
        private bool isLoading = false;
        public bool IsLoading
        {
            get { return isLoading; }
            set
            {
                isLoading = value;
                RaisePropertyChanged("IsLoading");
            }
        }

        private ObservableCollection<Payload> news;
        public ObservableCollection<Payload> News
        {
            get { return news; }
            set
            {
                news = value;
                RaisePropertyChanged("News");
            }
        }

        private RelayCommand refreshCommand;
        public ICommand RefreshCommand
        {
            get
            {
                return refreshCommand ?? (refreshCommand = new RelayCommand(async param => await RefreshAsync()));
            }
        }

        private RelayCommand navigateCommand;
        public ICommand NavigateCommand
        {
            get
            {
                return navigateCommand ?? (navigateCommand = new RelayCommand(param => NavigateCommandAction(param)));
            }
        }

        public async Task RefreshAsync()
        {
            IsLoading = true;

            try
            {
                if (!CacheService.IsInitialized)
                    CacheService.Initialize();
            }
            catch
            {
                Windows.UI.Popups.MessageDialog err = new Windows.UI.Popups.MessageDialog("Ошибка инициалиации кэша");
                await err.ShowAsync();
            }

            List<Payload> news = new List<Payload>();
            bool isSuccessLoad = false;
            try
            {
                news = await DataService.LoadNewsAsync();
                isSuccessLoad = true;
            }
            catch (HttpRequestException)
            {
                news = await LoadNewsFromCache();
            }
            catch
            {
                Windows.UI.Popups.MessageDialog err = new Windows.UI.Popups.MessageDialog("Ошибка загрузки новостей");
                await err.ShowAsync();
                news = await LoadNewsFromCache();
            }

            if (News == null)
            {
                News = new ObservableCollection<Payload>(news);
            }
            else
            {
                foreach (var item in news.Except(News))
                {
                    News.Insert(0, item);
                }
            }
            
            if (isSuccessLoad)
            {
                try
                {
                    CacheService.Update(news);
                }
                catch
                {
                    Windows.UI.Popups.MessageDialog err = new Windows.UI.Popups.MessageDialog("Ошибка обновления кэша");
                    await err.ShowAsync();
                }
            }

            IsLoading = false;
        }

        private void NavigateCommandAction(object param)
        {
            Payload payload = param as Payload;
            if (payload != null)
            {
                _navigationService.NavigateTo("NewsTextPage", payload.id);
            }
            
        }

        private readonly INavigationService _navigationService;
        public MainPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }
        private async Task<List<Payload>> LoadNewsFromCache()
        {
            List<Payload> result = new List<Payload>();
            try
            {
                result = CacheService.LoadNews();
            }
            catch
            {
                Windows.UI.Popups.MessageDialog err = new Windows.UI.Popups.MessageDialog("Ошибка загрузки новостей из кэша");
                await err.ShowAsync();
            }
            return result;
        }

        public async void Activate(object parameter)
        {
            if (News == null)
                await RefreshAsync();
        }

        public void Deactivate(object parameter)
        {
            //Do nothing
        }
    }
}
