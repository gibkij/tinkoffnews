﻿using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TinkoffNews.Extension;
using TinkoffNews.Model;
using TinkoffNews.Service;

namespace TinkoffNews.ViewModel
{
    class NewsTextViewModel : ViewModelBase, INavigable
    {
        private bool isLoading = false;
        public bool IsLoading
        {
            get { return isLoading; }
            set
            {
                isLoading = value;
                RaisePropertyChanged("IsLoading");
            }
        }

        private string content;
        public string Content
        {
            get { return content; }
            set
            {
                content = value;
                RaisePropertyChanged("Content");
            }
        }

        public async Task RefreshAsync(int newsId)
        {
            IsLoading = true;

            bool isSuccessLoad = false;
            Payload payload = null;
            try
            {
                payload = await DataService.LoadSingleNewsAsync(newsId);
                isSuccessLoad = true;
            }
            catch (HttpRequestException)
            {
                payload = await LoadSingleNewsFromCache(newsId);
            }
            catch
            {
                Windows.UI.Popups.MessageDialog err = new Windows.UI.Popups.MessageDialog("Ошибка загрузки новости");
                await err.ShowAsync();
                payload = await LoadSingleNewsFromCache(newsId);
            }

            if (payload != null)
            {
                string content = payload.content ?? String.Empty;
                string title = payload.title?.text ?? String.Empty;
                content = "<b><p>" + title + "</p></b>" + content;
                Content = content.Replace("<p>", "<p style=\"font-family: 'Segoe UI'; font-size: 16px\">");
            }

            if (isSuccessLoad)
            {
                try
                { 
                    CacheService.Update(payload);
                }
                catch
                {
                    Windows.UI.Popups.MessageDialog err = new Windows.UI.Popups.MessageDialog("Ошибка обновления кэша");
                    await err.ShowAsync();
                }
            }
            IsLoading = false;
        }

        private async Task<Payload> LoadSingleNewsFromCache(int newsId)
        {
            Payload result = null;
            try
            {
                result = CacheService.LoadSingleNews(newsId);
            }
            catch
            {
                Windows.UI.Popups.MessageDialog err = new Windows.UI.Popups.MessageDialog("Ошибка загрузки новости из кэша");
                await err.ShowAsync();
            }
            
            return result;
        }

        public async void Activate(object parameter)
        {
            int id;
            string idStr = parameter as string;
            if (!String.IsNullOrEmpty(idStr) && Int32.TryParse(idStr, out id))
            {
                await RefreshAsync(id);
            }
        }

        public void Deactivate(object parameter)
        {
            //Do nothing
        }
    }
}
