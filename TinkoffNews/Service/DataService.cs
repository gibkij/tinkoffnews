﻿using Newtonsoft.Json;
using SQLite.Net;
using SQLite.Net.Async;
using SQLite.Net.Platform.WinRT;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TinkoffNews.Model;
using Windows.Storage;

namespace TinkoffNews.Service
{
    class DataService
    {
        private static readonly string newsApiUrl = @"https://api.tinkoff.ru/v1/news";
        private static readonly string singleNewsApiUrl = @"https://api.tinkoff.ru/v1/news_content?id={0}";
        public async static Task<List<Payload>> LoadNewsAsync()
        {
            List<Payload> result = new List<Payload>();
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(newsApiUrl);

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    json = ExcludeEscapeSymbols(json);

                    var root = JsonConvert.DeserializeObject<NewsRootObject>(json);

                    result = root?.payload;

                    if (result != null)
                    {
                        //Remove english posts
                        result.RemoveAll(p => p.bankInfoTypeId == 1);

                        //Sorting by publishing time
                        result = result.OrderByDescending(p => p.publicationDate.milliseconds).ToList();
                    }
                }
            }
            return result;
        }

        public static async Task<Payload> LoadSingleNewsAsync(int newsId)
        {
            Payload result = null;
            using (HttpClient client = new HttpClient())
            {
                string uri = String.Format(singleNewsApiUrl, newsId);
                HttpResponseMessage response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var root = JsonConvert.DeserializeObject<SingleNewsRootObject>(json);

                    result = root?.payload;

                    if (result?.title?.text != null)
                    {
                        result.title.text = ExcludeEscapeSymbols(result.title.text);
                    }
                }
            }
            return result;
        }

        private static string ExcludeEscapeSymbols(string input)
        {
            return input.Replace("&nbsp;", " ").Replace("&laquo;", "«")
                .Replace("&raquo;", "»").Replace("<nobr>", "")
                .Replace("</nobr>", "").Replace("&mdash;", "-");
        }
    }
}
