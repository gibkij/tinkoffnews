﻿using SQLite.Net;
using SQLite.Net.Platform.WinRT;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinkoffNews.Model;
using Windows.Storage;

namespace TinkoffNews.Service
{
    class CacheService
    {
        private static readonly string dbname = "storage";

        private static List<Payload> payloadCache;
        private static object lockCache = new object();

        private static bool isInitialized = false;
        public static bool IsInitialized
        {
            get { return isInitialized; }
            set { isInitialized = value; }
        }

        public static void Initialize()
        {
            using (var db = new SQLiteConnection(new SQLitePlatformWinRT(),
                Path.Combine(ApplicationData.Current.LocalFolder.Path, dbname)))
            {
                db.CreateTable<PayloadEntity>();
                Refresh();
            }
            IsInitialized = true;
        }

        private static void Refresh()
        {
            using (var db = new SQLiteConnection(new SQLitePlatformWinRT(),
                    Path.Combine(ApplicationData.Current.LocalFolder.Path, dbname)))
            {
                lock (lockCache)
                {
                    payloadCache = db.Table<PayloadEntity>().Select(p => new Payload
                    {
                        id = p.Id,
                        publicationDate = new PublicationDate
                        {
                            milliseconds = p.PublicationDate
                        },
                        text = p.Text
                    }).OrderByDescending(p => p.publicationDate.milliseconds).ToList();
                }
            }
        }
        public static List<Payload> LoadNews()
        {
            List<Payload> result = null;
            lock(lockCache)
            {
                result = new List<Payload>(payloadCache);
            }
            return result;
        }

        public static Payload LoadSingleNews(int newsId)
        {
            Payload result = null;
            using (var db = new SQLiteConnection(new SQLitePlatformWinRT(),
                    Path.Combine(ApplicationData.Current.LocalFolder.Path, dbname)))
            {
                string idStr = newsId.ToString();
                result = db.Table<PayloadEntity>().Where(p => p.Id == idStr).Select(p => new Payload
                {
                    id = p.Id,
                    publicationDate = new PublicationDate { milliseconds = p.PublicationDate },
                    content = p.Content,
                    title = new Title { text = p.Text }
                }).SingleOrDefault();
            }
            return result;
        }

        public static void Update(List<Payload> news)
        {
            lock (lockCache)
            {
                var newItems = news.Except(payloadCache);
                if (newItems.Count() != 0)
                {
                    List<PayloadEntity> newPayloadEntities = new List<PayloadEntity>();
                    foreach (var item in newItems)
                    {
                        newPayloadEntities.Add(new PayloadEntity { Id = item.id, Text = item.text, PublicationDate = item.publicationDate.milliseconds });
                    }

                    using (var db = new SQLiteConnection(new SQLitePlatformWinRT(),
                        Path.Combine(ApplicationData.Current.LocalFolder.Path, dbname)))
                    {
                        db.InsertAll(newPayloadEntities);
                    }
                    payloadCache.AddRange(newItems);
                }
            }
        }

        public static void Update(Payload news)
        {
            using (var db = new SQLiteConnection(new SQLitePlatformWinRT(),
                    Path.Combine(ApplicationData.Current.LocalFolder.Path, dbname)))
            {
                db.InsertOrReplace(new PayloadEntity
                {
                    Id = news.title.id,
                    Content = news.content,
                    Text = news.title.text,
                    PublicationDate = news.title.publicationDate.milliseconds
                });
            }
        }
    }
}
