﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinkoffNews.Model
{
    class NewsRootObject
    {
        public string resultCode { get; set; }
        public List<Payload> payload { get; set; }
    }

    class SingleNewsRootObject
    {
        public string resultCode { get; set; }
        public Payload payload { get; set; }
    }
}
