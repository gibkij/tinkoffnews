﻿using SQLite.Net.Attributes;
using System;

namespace TinkoffNews.Model
{
    class Payload
    {
        public string id { get; set; }
        public string name { get; set; }
        public string text { get; set; }
        public PublicationDate publicationDate { get; set; }
        public int bankInfoTypeId { get; set; }
        public string content { get; set; }
        public Title title { get; set; }
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            Payload payload = obj as Payload;
            if (payload == null)
            {
                return false;
            }
            return id == payload.id;
        }

        public override int GetHashCode()
        {
            return id == null ? 0 : id.GetHashCode();
        }

    }

    class PayloadEntity
    {
        [PrimaryKey]
        public string Id { get; set; }
        public string Text { get; set; }
        public long PublicationDate { get; set; }
        public string Content { get; set; }
    }
}