﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinkoffNews.Model
{
    class Title
    {
        public string id { get; set; }
        public string name { get; set; }
        public string text { get; set; }
        public PublicationDate publicationDate { get; set; }
        public int bankInfoTypeId { get; set; }
    }
}
