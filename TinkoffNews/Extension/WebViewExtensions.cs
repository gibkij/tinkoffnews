﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace TinkoffNews.Extension
{
    public static class WebViewExtensions
    {
        public static string GetStringSource(WebView view)
        {
            return (string)view.GetValue(StringSourceProperty);
        }

        public static void SetStringSource(WebView view, string value)
        {
            view.SetValue(StringSourceProperty, value);
        }

        public static readonly DependencyProperty StringSourceProperty =
            DependencyProperty.RegisterAttached(
            "StringSource", typeof(string), typeof(WebViewExtensions),
            new PropertyMetadata(null, OnStringSourcePropertyChanged));

        private static void OnStringSourcePropertyChanged(DependencyObject sender,
            DependencyPropertyChangedEventArgs e)
        {
            var webView = sender as WebView;
            if (webView == null)
                throw new NotSupportedException();

            if (e.NewValue != null)
            {
                var text = e.NewValue.ToString();
                webView.NavigateToString(text);
            }
        }
    }
}
